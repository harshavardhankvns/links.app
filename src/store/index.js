import Vue from "vue";
import Vuex from "vuex";
import { icons, profileTypes } from "../Constants";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: {
      Id: 1,
      Name: "Yogi",
    },
    profiles: [
      {
        Id: 1,
        Name: "Instagram",
        Link: "https://www.instagram.com/yogigupta_tech/",
      },
      {
        Id: 6,
        Name: "LinkedIn",
        Link: "https://www.linkedin.com/in/yoganrusimhaa/",
      },
      {
        Id: 1,
        Name: "Instagram",
        Link: "https://www.instagram.com/yogigupta_tech/",
      },
      {
        Id: 6,
        Name: "LinkedIn",
        Link: "https://www.linkedin.com/in/yoganrusimhaa/",
      },
      {
        Id: 1,
        Name: "Instagram",
        Link: "https://www.instagram.com/yogigupta_tech/",
      },
      {
        Id: 6,
        Name: "LinkedIn",
        Link: "https://www.linkedin.com/in/yoganrusimhaa/",
      },
    ],
    customLinks: [
      {
        Id: 1,
        text: "Hi prends, visit my website. Bye prends",
        Link: "https://www.instagram.com/yogigupta_tech/",
      },
      {
        Id: 6,
        text: "Hi prends, visit my website. Bye prends asdfg asdfgh asdfg asdfg",
        Link: "https://www.linkedin.com/in/yoganrusimhaa/",
      },
      {
        Id: 1,
        text: "Hi prends, visit my website. Bye prends",
        Link: "https://www.instagram.com/yogigupta_tech/",
      },
      {
        Id: 6,
        text: "Hi prends, visit my website. Bye prends",
        Link: "https://www.linkedin.com/in/yoganrusimhaa/",
      },
      {
        Id: 1,
        text: "Hi prends, visit my website. Bye prends",
        Link: "https://www.instagram.com/yogigupta_tech/",
      },
      {
        Id: 6,
        text: "Hi prends, visit my website. Bye prends",
        Link: "https://www.linkedin.com/in/yoganrusimhaa/",
      },
      {
        Id: 1,
        text: "Hi prends, visit my website. Bye prends jvckjhdowasd lfasdaihaewofishi oifweakaelsfdhqw ",
        Link: "https://www.instagram.com/yogigupta_tech/",
      },
      {
        Id: 6,
        text: "Hi prends, visit my website. Bye prends",
        Link: "https://www.linkedin.com/in/yoganrusimhaa/",
      },
    ],
  },
  getters: {
    currentUser(state) {
      return state.user;
    },
    profiles(state) {
      var profiles = [];
      state.profiles.forEach((p) => {
        p.icon = icons[p.Id - 1].name;
        profiles.push(p);
      });
      return profiles;
    },
    customLinks(state) {
      var customLinks = [];
      state.customLinks.forEach((c) => {
        c.icon = icons[c.Id - 1].name;
        customLinks.push(c);
      });
      return customLinks;
    },
    allProfiles() {
      var allProfiles = [];
      profileTypes.forEach((p) => {
        p.icon = icons[p.Id - 1].name;
        allProfiles.push(p);
      });
      return allProfiles;
    },
  },
  mutations: {},
  actions: {},
  modules: {},
});
