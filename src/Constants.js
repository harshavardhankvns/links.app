export const icons = [
  {
    profileId: 1,
    name: "mdi-instagram",
  },
  {
    profileId: 2,
    name: "mdi-twitter",
  },
  {
    profileId: 3,
    name: "mdi-facebook",
  },
  {
    profileId: 4,
    name: "mdi-youtube",
  },
  {
    profileId: 5,
    name: "mdi-snapchat",
  },
  {
    profileId: 6,
    name: "mdi-linkedin",
  },
  {
    profileId: 7,
    name: "mdi-telegram",
  },
  {
    profileId: 8,
    name: "mdi-discord",
  },
  {
    profileId: 9,
    name: "mdi-reddit",
  },
  {
    profileId: 10,
    name: "mdi-github",
  },
  {
    profileId: 11,
    name: "mdi-pinterest",
  },
  {
    profileId: 12,
    name: "mdi-twitch",
  },
  {
    profileId: 13,
    name: "mdi-spotify",
  },
  {
    profileId: 14,
    name: "mdi-tinder",
  },
  {
    profileId: 15,
    name: "mdi-tiktok",
  },
  {
    profileId: 16,
    name: "mdi-koo",
  },
  {
    profileId: 17,
    name: "mdi-quora",
  },
  {
    profileId: 18,
    name: "mdi-stack-overflow",
  },
  {
    profileId: 19,
    name: "mdi-stack-exchange",
  },
  {
    profileId: 20,
    name: "mdi-clubhouse",
  },
];

export const profileTypes = [
  {
    Id: 1,
    name: "Instagram",
  },
  {
    Id: 2,
    name: "Twitter",
  },
  {
    Id: 3,
    name: "Facebook",
  },
  {
    Id: 4,
    name: "Youtube",
  },
  {
    Id: 5,
    name: "Snapchat",
  },
  {
    Id: 6,
    name: "LinkedIn",
  },
  {
    Id: 7,
    name: "Telegram",
  },
  {
    Id: 8,
    name: "Discord",
  },
  {
    Id: 9,
    name: "Reddit",
  },
  {
    Id: 10,
    name: "Github",
  },
  {
    Id: 11,
    name: "Pinterest",
  },
  {
    Id: 12,
    name: "Twitch",
  },
  {
    Id: 13,
    name: "Spotify",
  },
  {
    Id: 14,
    name: "Tinder",
  },
  {
    Id: 15,
    name: "TikTok",
  },
  {
    Id: 16,
    name: "Koo",
  },
  {
    Id: 17,
    name: "Quora",
  },
  {
    Id: 18,
    name: "Stack Overflow",
  },
  {
    Id: 19,
    name: "Stack Exchange",
  },
  {
    Id: 20,
    name: "Clubhouse",
  },
];
